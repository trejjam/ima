/* 
 * File:   crawler.cpp
 * Author: jam
 * 
 * Created on 25. listopad 2014, 23:23
 */

#include "Crawler.hpp"

#include <string>
#include <cstring>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
//#include <unistd.h>

//#include <stdio.h>
//#include <stdlib.h>


using namespace std;

int retRun;
void *Run(void *threadArg)
{    
    struct crawlerData *myData;
    myData = (struct crawlerData *) threadArg;
    try {
        retRun = myData->crawler->start();
    }
    catch(crawlerException e) {
        cout<<"--"<<e.errNo <<" - "<<e.errMessage<<endl;
    }
    catch(...) {
        cout <<"--"<<"error!"<<endl;
    }
    cout<<"--"<<"Ukončeno vlákno CrawlerThread"<<endl;
    pthread_exit(&retRun);
}

Crawler::Crawler(IStorage *storage, string host, int port) {
    pthread_attr_t attr;
    int rc;
    
    // Initialize and set thread joinable
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    cout <<"-"<<"Crawler: creating CrawlerThread" << endl;
    crawler=new CrawlerThread(host, port, storage);
    cd.crawler=crawler;
    
    rc = pthread_create(&thread, NULL, &Run, (void *)&cd);
    if (rc){
        cout <<"-"<< "Error: unable to create thread: " << rc << endl;
    }
    else {
        cout <<"-"<< "CrawlerThread created" << rc << endl;
    }
    
    pthread_attr_destroy(&attr);
}
Crawler::~Crawler() {
    int *status;
    int rc;
    
    cout <<"-"<< "Zasílání stop: CrawlerThread"<<endl;
    crawler->setRun(false);
    
    cout <<"-"<< "Čekání na ukončení CrawlerThread"<<endl;
    
    rc = pthread_join(thread, (void **)&status);
    
    cout <<"-"<< "CrawlerThread ukončen stavem: " << *status << endl;
    
    delete crawler;
}
