//libmysqlcppconn-dev
//http://dev.mysql.com/doc/connector-cpp/en/connector-cpp-examples-complete-example-1.html
//http://dev.mysql.com/doc/connector-cpp/en/connector-cpp-examples-complete-example-2.html

#include "SqlStorage.hpp"

#include <iostream>

#include "mysql_connection.h"
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>


using namespace std;

const string SqlStorage::database = "ima";
const string SqlStorage::logTable = "log";
const string SqlStorage::tagTable = "log__tag";
const string SqlStorage::logMeasurementTable = "log__measurements";
const string SqlStorage::tagHistoryTable = "maps__tag_learn_history";

const int SqlStorage::MAX_PREP_STATEMENT = 10000;

SqlStorage::SqlStorage(const sql::SQLString& hostName, const sql::SQLString& userName, const sql::SQLString& password) {
    measurementId = 0;

    this->hostName = hostName;
    this->userName = userName;
    this->password = password;

    connect(hostName, userName, password);
}

SqlStorage::~SqlStorage() {
    cout << "Destruct SQL conn" << endl;

    delete con;
}

void SqlStorage::reconect() {
    isPreparedStatements = false;
    delete con;

    cout << "---------------SQL reconnect: " << endl;

    connect(hostName, userName, password);
}

void SqlStorage::connect(const sql::SQLString& hostName, const sql::SQLString& userName, const sql::SQLString& password) {
    sql::Driver *driver;

    isPreparedStatements = false;
    preparedStatementCount = 0;

    try {
        driver = get_driver_instance();
        con = driver->connect(hostName, userName, password);
        con->setSchema(database);
    } catch (sql::SQLException &e) {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
}

std::string SqlStorage::addSlashes(std::string key) {
    return "`" + key + "`";
}

std::string SqlStorage::addSlashes(std::string* key, int length) {
    string tmp = "";

    for (int i = 0; i < length; i++) {
        if (tmp.length() > 0) tmp += ", ";
        tmp += addSlashes(key[i]);
    }

    return tmp;
}

void SqlStorage::PrepareStatements() {
    const unsigned int keysLength = 9;
    const string _transmitter = "transmitter";
    const string _tagId = "tag_id";
    const string _receiver = "receiver";
    const string _packet = "packet";
    const string _signalStrength = "signal_strength";
    const string _button = "button";
    const string _timestamp = "timestamp";
    const string _learnRoomId = "learn_room_id";
    const string _measurementId = "measurement_id";

    const string _tagNumber = "tag_number";
    const string _roomId = "room_id";

    string tableKeys[keysLength];
    tableKeys[0] = _transmitter;
    tableKeys[1] = _tagId;
    tableKeys[2] = _receiver;
    tableKeys[3] = _packet;
    tableKeys[4] = _signalStrength;
    tableKeys[5] = _button;
    tableKeys[6] = _timestamp;
    tableKeys[7] = _learnRoomId;
    tableKeys[8] = _measurementId;

    struct preparedStatements preparedStatementsInit;

    preparedStatementCount++;
    preparedStatementsInit.pstmtGetTagId = con->prepareStatement("SELECT id FROM " + addSlashes(tagTable) + " WHERE " + addSlashes(_tagNumber) + "=? limit 1");
    preparedStatementCount++;
    preparedStatementsInit.pstmtSetTagId = con->prepareStatement("INSERT INTO " + addSlashes(tagTable) + " (" + addSlashes(_tagNumber) + ") VALUES(?)");
    preparedStatementCount++;
    preparedStatementsInit.pstmtGetRoomId = con->prepareStatement("SELECT " + addSlashes(_roomId) + " FROM " + addSlashes(tagHistoryTable) + " WHERE " + addSlashes(_tagId) + "=? ORDER BY `id` DESC LIMIT 1");
    preparedStatementCount++;
    preparedStatementsInit.pstmtSave = con->prepareStatement("INSERT INTO " + addSlashes(logTable) + "(" + addSlashes(tableKeys, keysLength) + ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
    preparedStatementCount++;
    preparedStatementsInit.pstmtSetMeasurement = con->prepareStatement("INSERT INTO " + addSlashes(logMeasurementTable) + "(" + addSlashes("info") + ") VALUES (?)");

    preparedStatements = preparedStatementsInit;

    isPreparedStatements = true;
}

bool SqlStorage::Save(double timestamp, unsigned int receiver, unsigned int transmitter, unsigned int packet, unsigned int signalStrength, unsigned int button) {
    if (MAX_PREP_STATEMENT < preparedStatementCount) {
        reconect();
    }
    if (!isPreparedStatements) {
        PrepareStatements();
    }
    cout << preparedStatementCount << " ";


    sql::ResultSet *result;

    try {
        int tagId = 0;
        int roomId = 0;

        preparedStatements.pstmtGetTagId->setUInt(1, transmitter);
        result = preparedStatements.pstmtGetTagId->executeQuery();

        if (!result->next()) {
            preparedStatements.pstmtSetTagId->setUInt(1, transmitter);
            preparedStatements.pstmtSetTagId->executeUpdate();

            sql::Statement *stmt = con->createStatement();

            result = stmt->executeQuery("SELECT LAST_INSERT_ID()");
            if (result->next()) {
                tagId = result->getInt(1);
            }

            delete stmt;
        } else {
            tagId = result->getInt(1);

            preparedStatements.pstmtGetRoomId->setUInt(1, tagId);
            result = preparedStatements.pstmtGetRoomId->executeQuery();
            if (result->next()) {
                roomId = result->getInt(1);
            }
        }

        preparedStatements.pstmtSave->setUInt(1, transmitter);
        if (tagId != 0) preparedStatements.pstmtSave->setUInt(2, tagId);
        else preparedStatements.pstmtSave->setNull(2, sql::DataType::INTEGER);
        preparedStatements.pstmtSave->setUInt(3, receiver);
        preparedStatements.pstmtSave->setUInt(4, packet);
        preparedStatements.pstmtSave->setUInt(5, signalStrength);
        preparedStatements.pstmtSave->setUInt(6, button);
        preparedStatements.pstmtSave->setDouble(7, timestamp);
        if (roomId != 0) preparedStatements.pstmtSave->setUInt(8, roomId);
        else preparedStatements.pstmtSave->setNull(8, sql::DataType::INTEGER);
        if (measurementId != 0) preparedStatements.pstmtSave->setUInt(9, measurementId);
        else preparedStatements.pstmtSave->setNull(9, sql::DataType::INTEGER);

        preparedStatements.pstmtSave->executeUpdate();
    } catch (sql::SQLException &e) {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }

    delete result;

    return true;
}

void SqlStorage::setMeasurement(std::string info) {
    if (!isPreparedStatements) {
        PrepareStatements();
    }
    
    preparedStatements.pstmtSetMeasurement->setString(1, info);
    preparedStatements.pstmtSetMeasurement->executeUpdate();

    sql::Statement *stmt = con->createStatement();
    sql::ResultSet *result;

    result = stmt->executeQuery("SELECT LAST_INSERT_ID()");
    if (result->next()) {
        measurementId = result->getInt(1);
    }

    delete stmt;
    delete result;
}
