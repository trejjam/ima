/* 
 * File:   crawler.hpp
 * Author: jam
 *
 * Created on 25. listopad 2014, 23:23
 */
#ifndef CRAWLER_HPP
#define	CRAWLER_HPP

#include "IStorage.hpp"
#include "CrawlerThread.hpp"

#include <iostream>
#include <pthread.h>

struct crawlerException {
    int errNo;
    std::string errMessage;
};
struct crawlerData {
    CrawlerThread *crawler;
};

class Crawler {
    private:
        struct crawlerData cd;
        pthread_t thread;
        CrawlerThread *crawler;
    public:
        Crawler(IStorage *storage, std::string host, int port);
        Crawler(const Crawler& orig);
        ~Crawler();
};

#endif	/* CRAWLER_HPP */

