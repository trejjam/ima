/* 
 * File:   IStorage.hpp
 * Author: jam
 *
 * Created on 25. listopad 2014, 21:28
 */

#ifndef ISTORAGE_H
#define	ISTORAGE_H

#include <iostream>
#include <stdio.h>
#include <string>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <cstdio>

class IStorage
{
    public:
        virtual ~IStorage() { std::cout <<"Destruct IStorage" <<std::endl; };
        virtual bool Save(double timestamp, unsigned int receiver, unsigned int transmitter, unsigned int packet, unsigned int signalStrength, unsigned int button) = 0;
        virtual void setMeasurement(std::string info) = 0;
};

#endif	/* ISTORAGE_H */

