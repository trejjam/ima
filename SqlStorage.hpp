/* 
 * File:   SqlStorage.hpp
 * Author: jam
 *
 * Created on 25. listopad 2014, 21:35
 */

#ifndef SQLSTORAGE_H
#define	SQLSTORAGE_H

#include "IStorage.hpp"
#include "mysql_connection.h"

#include <iostream>

class SqlStorage : public IStorage
{
    private:
        struct preparedStatements {
            sql::PreparedStatement *pstmtGetTagId;
            sql::PreparedStatement *pstmtSetTagId;
            sql::PreparedStatement *pstmtGetRoomId;
            sql::PreparedStatement *pstmtSetMeasurement;
            sql::PreparedStatement *pstmtSave;
        };
        
        sql::Connection *con;
        static const std::string database;
        static const std::string logTable;
        static const std::string tagTable;
        static const std::string logMeasurementTable;
        static const std::string tagHistoryTable;
        int preparedStatementCount;
        sql::SQLString hostName;
        sql::SQLString userName;
        sql::SQLString password;
        
        static const int MAX_PREP_STATEMENT;
        preparedStatements preparedStatements;
        bool isPreparedStatements;
        
        std::string addSlashes(std::string key);
        std::string addSlashes(std::string* key, int length);
        void PrepareStatements(void);
        void reconect(void);
        int measurementId;
    public:
        SqlStorage(const sql::SQLString& hostName, const sql::SQLString& userName, const sql::SQLString& password);
        SqlStorage(const SqlStorage& orig);
        ~SqlStorage();
        bool Save(double timestamp, unsigned int receiver, unsigned int transmitter, unsigned int packet, unsigned int signalStrength, unsigned int button);
        void connect(const sql::SQLString& hostName, const sql::SQLString& userName, const sql::SQLString& password);
        void setMeasurement(std::string info);
};

#endif	/* SQLSTORAGE_H */

