/* 
 * File:   main.cpp
 * Author: jam
 *
 * Created on 23. listopad 2014, 20:57
 */

#include <stdio.h>
#include <string>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <cstdio>

#include "SqlStorage.hpp"
#include "Crawler.hpp"

using namespace std;

/*
 * 
 */
int main(int argc, const char *argv[]) {
    bool run=true;
    
    string host;
    int port;
    string dbHost="localhost";
    string dbUser, dbPass;
    int dbPort=3306;
    string arg, argType, argType3;
    
    for (int i=0; i<argc; i++) {        
        arg=argv[i];
        argType=arg.substr(0,2);
        argType3=arg.substr(0,3);
        
        if(argType=="-h" && arg.length()>3) {
            host=arg.substr(3, arg.length()-3);
        }
        else if(argType=="-p" && arg.length()>3) {
            sscanf(arg.c_str(), "-p=%i", &port);
        }
        else if(argType3=="-dh" && arg.length()>3) {
            dbHost=arg.substr(4, arg.length()-4);
        }
        else if(argType3=="-dp" && arg.length()>3) {
            sscanf(arg.c_str(), "-dp=%i", &dbPort);
        }
        else if(argType3=="-dU" && arg.length()>3) {
            dbUser=arg.substr(4, arg.length()-4);
        }
        else if(argType3=="-dP" && arg.length()>3) {
            dbPass=arg.substr(4, arg.length()-4);
        }
    }
    
    cout <<"Host: "<<host<<endl;
    cout <<"Port: "<<port<<endl;
    cout <<"Database: "<<endl;
    cout <<"Host: "<<dbHost<<endl;
    cout <<"Port: "<<dbPort<<endl;
    cout <<"User: "<<dbUser<<endl;
    cout <<"Password: "<<dbPass<<endl;
    
    IStorage *storage=new SqlStorage("tcp://localhost:3306", dbUser, dbPass);
    
    Crawler *crawler;
    crawler= new Crawler(storage, host, port);
    
    string input;
    string inputText;
    while(run) {
        cout <<"Zadejte příkaz:"<<endl;
        cin >>input;
        
        if (input=="q") {
            run=false;
        }
        else if (input=="m") {
            cin >>inputText;
                        
            storage->setMeasurement(inputText);
        }
    }
    
    delete crawler;
    delete storage;    
    
    return 0;
}

