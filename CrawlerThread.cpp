//http://www.builder.cz/rubriky/c/c--/tcp-klient-v-linuxu-156204cz
/* 
 * File:   CrawlerThread.cpp
 * Author: jam
 * 
 * Created on 26. listopad 2014, 14:04
 */

#include "CrawlerThread.hpp"
#include "Crawler.hpp"

#include <iostream>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h>
#include <cstdio>

using namespace std;

CrawlerThread::CrawlerThread(string host, int port, IStorage *storage):host(host), port(port), storage(storage) {
    pthread_mutex_init(&mutex_run, NULL);
    debug=false;
    
    setRun(true);
}
CrawlerThread::~CrawlerThread() {
    cout <<"--"<<"Destruct CThread"<<endl;
}

void CrawlerThread::setRun(bool run_) {
    pthread_mutex_lock( &mutex_run );
    run=run_;
    pthread_mutex_unlock( &mutex_run );
}
bool CrawlerThread::getRun(void) {
    pthread_mutex_lock( &mutex_run );
    bool run_=run;
    pthread_mutex_unlock( &mutex_run );
    
    return run_;
}
int CrawlerThread::start(void) {
    int status=1; 
    
    cout <<"--"<<"run " <<endl; 
    
    sockaddr_in serverSock;
    int socket;            
    string recieved;
    
    serverSock=prepareConn(CrawlerThread::host, CrawlerThread::port);
    socket=createSocket(1);
    
    connect(socket, serverSock);
    
    while(getRun()) {     
        try {
            recieved=recieve(socket);

            collectData(recieved);
            if (debug) cout <<"--"<<"recieved: "<<recieved <<endl;
        }
        catch (emptyException e) { }
        usleep(100);
    }
    cout<<"--"<<"Closing"<<endl;
    close(socket);
    cout<<"--"<<"Closed"<<endl;
    
    return status;
}
hostent* CrawlerThread::getHostent(std::string host) {
    hostent* hostent_;
    // Zjistíme info o vzdáleném počítači
    if ((hostent_ = gethostbyname(host.c_str())) == NULL)
    {
        crawlerException e;
        e.errNo=-1;
        e.errMessage="Špatný formát IP adresy";
        throw e;
    }
    
    return hostent_;
}
sockaddr_in CrawlerThread::prepareConn(std::string host, int port) {
    hostent *hostent_=getHostent(host);
    sockaddr_in serverSock;
    
    //sockaddr_in serverSock;
    // Zaplníme strukturu sockaddr_in
    // 1) Rodina protokolů
    serverSock.sin_family = AF_INET;
    // 2) Číslo portu, ke kterému se připojíme
    serverSock.sin_port = htons(port);
    // 3) Nastavení IP adresy, ke které se připojíme
    memcpy(&(serverSock.sin_addr), hostent_->h_addr, hostent_->h_length);
    
    return serverSock;
}
int CrawlerThread::createSocket() {
    int socket;
    // Vytvoříme soket
    if ((socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
    {
        crawlerException e;
        e.errNo=-2;
        e.errMessage="Nelze vytvořit soket";
        throw e;
    }
    return socket;
}
int CrawlerThread::createSocket(int timetout) {
    int socket=createSocket();
    
    struct timeval tv;
    tv.tv_sec = timetout;
    tv.tv_usec = 0;
    
    if (setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof tv)) 
    { 
        crawlerException e;
        e.errNo=-5;
        e.errMessage="Nelze nastavit soket";
        throw e;
    }
    
    return socket;
}
void CrawlerThread::connect(int socket, sockaddr_in sock) {
    int conn=::connect(socket, (sockaddr *)&sock, sizeof(sock));
    if (conn == -1)
    {
        crawlerException e;
        e.errNo=-3;
        e.errMessage="Nelze navázat spojení";
        throw e;
    }
}
string CrawlerThread::recieve(int socket) {
    int size = recv(socket, recieveBuffer, BUFSIZE, 0);
    if (size> 0) {
        return string(recieveBuffer, size);
    }
    
    emptyException e;
    e.errNo=-1;
    throw e;
}
void CrawlerThread::collectData(string data) {
    proceedBuffer+=data;
    
    proceedData();
}
void CrawlerThread::proceedData() {
    size_t found = proceedBuffer.find(DELIMITER);
    if (found!=string::npos) {
        string data = proceedBuffer.substr(0,found);

        parseData(data);
        
        cout <<data<<endl;        
        
        data.clear();
        
        proceedBuffer.replace(0,found+1,"");
        
        proceedData();
    }
}
void CrawlerThread::parseData(string data) {
    double time;
    char type;
    
    sscanf(data.c_str(), "%lf %c", &time, &type);
    
    if (type=='D') {
        unsigned int    transmitter,
                        receiver,
                        packet,
                        signalStrength,
                        button;
        
        sscanf(data.c_str(), "%lf %c %*s %x %x %x %x %d", &time, &type, &receiver, &transmitter, &packet, &signalStrength, &button);    
                
        storage->Save(time, receiver, transmitter, packet, signalStrength, button);
    }
}