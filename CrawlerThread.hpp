/* 
 * File:   CrawlerThread.hpp
 * Author: jam
 *
 * Created on 26. listopad 2014, 14:04
 */

#ifndef CRAWLERTHREAD_HPP
#define	CRAWLERTHREAD_HPP

#include "IStorage.hpp"

#include <string>
#include <cstring>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>

#define BUFSIZE 1000
#define DELIMITER '\n'

struct emptyException {
    int errNo;
};

class CrawlerThread {
    private:    
        IStorage *storage;
        bool run;
        bool debug;
        pthread_mutex_t mutex_run;
        
        std::string host;
        int port;
        
        hostent* getHostent(std::string host);
        sockaddr_in prepareConn(std::string host, int port);
        int createSocket();
        int createSocket(int timetout);
        void connect(int socket, sockaddr_in sock);
        
        char recieveBuffer[BUFSIZE];
        std::string recieve(int socket);
        
        std::string proceedBuffer;
        void collectData(std::string data);
        void proceedData(void);
        void parseData(std::string data);
    public:
        CrawlerThread(std::string host, int port, IStorage *storage);
        CrawlerThread(const CrawlerThread& orig);
        virtual ~CrawlerThread();
        
        void setRun(bool run);
        bool getRun(void);
        int start(void);
};

#endif	/* CRAWLERTHREAD_HPP */

